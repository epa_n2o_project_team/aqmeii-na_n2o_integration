#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# description
# ----------------------------------------------------------------------

# A top-level driver for integrating separate source inventories of N2O emissions with an existing CMAQ AQ EI over the same horizontal domain.

# See https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration

# Expects to run on linux. Utility dependencies include:

# * definitely

# ** bash: required to run this script. Known to work with bash --version==3.2.25

# * potentially initially. If you have not already retrieved regrid_utils (see `get_regrid_utils`), you will need

# ** git: to clone their repo
# ** web access (HTTP currently)

#   These must be available to the script when initially run.

# * ultimately

# ** basename, dirname
# ** cp
# ** curl or wget (latter {preferred, coded} for ability to deal with redirects)
# ** gunzip, unzip
# ** ncdump
# ** NCL
# ** R

#   Paths to the above can be setup in `bash_utilities::setup_paths`

# TODO: failing functions should fail this (entire) driver!

# Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(dirname ${THIS})"

### workspace
# note: following will fail if `source`ing!
# export WORK_DIR="${THIS_DIR}"          # moved to resource_strings.sh, to ...
source "${THIS_DIR}/resource_strings.sh" # allow reuse in N2O_integration_driver.sh

### downloading
WGET_TO_FILE='wget --no-check-certificate -c -O'
WGET_TO_STREAM="${WGET_TO_FILE} -"
CURL_TO_FILE='curl -C - -o'
CURL_TO_STREAM='curl'

### model/inventory constants
# export MODEL_YEAR='2008' moved to resource_strings
export MONTHS_PER_YEAR='12'
export HOURS_PER_DAY='24'
## where this is hosted
PROJECT_WEBSITE='https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration'
export PROJECT_HISTORY="see ${PROJECT_WEBSITE}"

# IOAPI metadata: see http://www.baronams.com/products/ioapi/INCLUDE.html#fdesc
export IOAPI_TIMESTEP_LENGTH='10000' # :TSTEP
export IOAPI_START_OF_TIMESTEPS='0'  # :STIME
export IOAPI_VARLIST_NAME='VAR-LIST' # attributes with dashes make NCL vomit

### numerical and math constants
export ZERO_MAX='0.0001' # any value less than this is probably 0
export ONE_MIN='0.9999'  # any value more than this is probably 1
export CMAQ_RADIUS='6370000' # of spherical earth, in meters
# pi, etc

### empirical constants
export MOLAR_MASS_N2O='44.0128' # grams per mole of N2O, per wolframalpha.com

### AQMEII-NA constants

## datavar dims and attributes
# TODO: get from template file or from EPIC
# "real" data
AQMEIINA_DV_NAME='N2O'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_DV_LONG_NAME="$(printf '%-16s' ${AQMEIINA_DV_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_DV_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
# Don't single-quote the payload: double-quote it (OK inside parens inside double-quotes)
AQMEIINA_DV_VAR_DESC="$(printf '%-80s' "Model species ${AQMEIINA_DV_NAME}")"

# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
AQMEIINA_TFLAG_NAME='TFLAG'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_TFLAG_LONG_NAME="$(printf '%-16s' ${AQMEIINA_TFLAG_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_TFLAG_UNITS="$(printf '%-16s' '<YYYYDDD,HHMMSS>')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
AQMEIINA_TFLAG_VAR_DESC="$(printf '%-80s' 'Timestep-valid flags:  (1) YYYYDDD or (2) HHMMSS')"

AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_LAYER_LONG_NAME='index of layers above surface'
AQMEIINA_DIM_LAYER_UNITS='unitless'

AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_TSTEP_UNITS='' # they vary!
AQMEIINA_DIM_TSTEP_LONG_NAME='timestep'

AQMEIINA_DIM_X_N='459'
AQMEIINA_DIM_X_NAME='COL'
# AQMEIINA_DIM_X_UNITS='unitless' # my invention
AQMEIINA_DIM_X_UNITS='m'
# AQMEIINA_DIM_X_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_X_LONG_NAME='grid-center offset from center of projection'

AQMEIINA_DIM_Y_N='299'
AQMEIINA_DIM_Y_NAME='ROW'
# AQMEIINA_DIM_Y_UNITS='unitless' # my invention
AQMEIINA_DIM_Y_UNITS='m'
# AQMEIINA_DIM_Y_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_Y_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"

# dimensions we don't really need, but VERDI does
AQMEIINA_DIM_DATETIME_NAME='DATE-TIME'
AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_VAR_NAME='VAR'

### artifact visualization

## for visualization (generally)
export OUTPUT_SIGNIFICANT_DIGITS='3' # see conservation report below
# PROJ.4 string for unprojected data
export GLOBAL_PROJ4='+proj=longlat +ellps=WGS84'

## for plotting (specifically)
# PDF_VIEWER='xpdf' # set this in bash_utilities::setup_apps
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${WORK_DIR}"
# dimensions (for R plots--units?) for single-frame plots
export SINGLE_FRAME_PLOT_HEIGHT='10'
export SINGLE_FRAME_PLOT_WIDTH='15'
# dimensions for multi-frame plots
export TWELVE_MONTH_PLOT_HEIGHT='120'
export TWELVE_MONTH_PLOT_WIDTH='20'

## conservation report constants
# need 5 additional digits for float output, e.g., "4.85e+04"
# bash arithmetic gotcha: allow no spaces around '='!
# export CONSERV_REPORT_FIELD_WIDTH=$((OUTPUT_SIGNIFICANT_DIGITS + 5))
export CONSERV_REPORT_FIELD_WIDTH='9' # width of 'AQMEII-NA'
export CONSERV_REPORT_FLOAT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}.$((OUTPUT_SIGNIFICANT_DIGITS - 1))e"
# echo -e "${THIS_FN}: CONSERV_REPORT_FLOAT_FORMAT=${CONSERV_REPORT_FLOAT_FORMAT}" # debugging
export CONSERV_REPORT_INT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}i"
export CONSERV_REPORT_COLUMN_SEPARATOR="  "
export CONSERV_REPORT_TITLE="Is N2O conserved from input to output? units=kg N2O"
export CONSERV_REPORT_SUBTITLE="(note (US land area)/(earth land area) ~= 6.15e-02)"

# ----------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------

### helpers in this repo

export SUM_N2O_FN='sum_N2O.ncl'
export SUM_N2O_FP="${WORK_DIR}/${SUM_N2O_FN}"
export N2O_PLUS_AQ_FN='N2O_plus_AQ.ncl'
export N2O_PLUS_AQ_FP="${WORK_DIR}/${N2O_PLUS_AQ_FN}"
export KLUDGE_SPINUP_FN='kludge_N2O_spinup.ncl'
export KLUDGE_SPINUP_FP="${WORK_DIR}/${KLUDGE_SPINUP_FN}"

### helpers retrieved from elsewhere. TODO: create NCL and R packages

# path to a project, not a .git
REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# can also use   'git@bitbucket.org:tlroche/regrid_utils' if supported
# path to a folder, not a file: needed by NCL to get to initial helpers
export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

export BASH_UTILS_FN='bash_utilities.sh'
# export BASH_UTILS_FP="${REGRID_UTILS_DIR}/${BASH_UTILS_FN}"
# no, allow user to override repo code: see `get_bash_utils`
export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

export FILEPATH_FUNCS_FN='get_filepath_from_template.ncl'
export FILEPATH_FUNCS_FP="${WORK_DIR}/${FILEPATH_FUNCS_FN}"

export IOAPI_FUNCS_FN='IOAPI.ncl'
export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

# export GET_AQMEIINA_AREAS_FN='get_output_areas.ncl'
# export GET_AQMEIINA_AREAS_FP="${WORK_DIR}/${GET_AQMEIINA_AREAS_FN}"

# export R_UTILS_FN='R_utilities.r'
# export R_UTILS_FP="${WORK_DIR}/${R_UTILS_FN}"

# export STATS_FUNCS_FN='netCDF.stats.to.stdout.r'
# export STATS_FUNCS_FP="${WORK_DIR}/${STATS_FUNCS_FN}"

export STRING_FUNCS_FN='string.ncl'
export STRING_FUNCS_FP="${WORK_DIR}/${STRING_FUNCS_FN}"

export SUMMARIZE_FUNCS_FN='summarize.ncl'
export SUMMARIZE_FUNCS_FP="${WORK_DIR}/${SUMMARIZE_FUNCS_FN}"

export TIME_FUNCS_FN='time.ncl'
export TIME_FUNCS_FP="${WORK_DIR}/${TIME_FUNCS_FN}"

# export VIS_FUNCS_FN='visualization.r'
# export VIS_FUNCS_FP="${WORK_DIR}/${VIS_FUNCS_FN}"

# ----------------------------------------------------------------------
# inputs
# ----------------------------------------------------------------------

### input: ag-soil hourly emissions
export AGSOIL_N2O_FP="${AGSOIL_TARGET_FP}" # from resource_strings.sh
## datavar
export AGSOIL_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"

### input: natural-soil hourly emissions
export NATSOIL_FP_TEMPLATE="${NATSOIL_TARGET_FP_TEMPLATE}"
## datavar
export NATSOIL_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"

### input: non-ag-soil anthropogenic hourly emissions
export NONSOIL_ANTHRO_N2O_FP="${NONSOIL_ANTHRO_TARGET_FP}"
## datavar
export NONSOIL_ANTHRO_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"

### input: marine hourly emissions
export MARINE_N2O_FP="${MARINE_TARGET_FP}"
## datavar
export MARINE_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"

### input: biomass-burning hourly emissions
export BURNING_FP_TEMPLATE="${BURNING_TARGET_FP_TEMPLATE}"
## datavar
export BURNING_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"

### input: AQ, non-N2O hourly emissions (previously used for PHASE/CDC run)
export AQ_FP_TEMPLATE="${AQ_TARGET_FP_TEMPLATE}"
## datavar name? NA: we'll be looping through many of them
# export AQ_DATAVAR_NAME=

# ----------------------------------------------------------------------
# products
# ----------------------------------------------------------------------

# TODO: move all this section to resource_strings.sh
# FN_ROOT_TEMPLATE="emis_mole_N2O_${TEMPLATE_STRING}_12US1_cmaq_cb05_soa_2008ab_08c"
# FN_TEMPLATE_NCL="${FN_ROOT_TEMPLATE}.${NETCDF_EXT_NCL}" # workaround NCL file-writing limitation

### daily sum of N2O hourlies
# # export DAILY_SUM_N2O_DIR="${WORK_DIR}"
# export DAILY_SUM_N2O_DIR='/work/MOD3APP/rtd/emis/2008N2O/AQMEII-NA_N2O_integration/N2O' # terrae
export DAILY_SUM_N2O_FP_TEMPLATE_NCL="${N2O_SUM_TARGET_DIR}/${N2O_SUM_SOURCE_FN_TEMPLATE}"
## datavar
export DAILY_SUM_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export DAILY_SUM_N2O_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
## dimensions
export DAILY_SUM_N2O_DIM_VAR_N='1' # for IOAPI: N2O is sole datavar
# file/global attributes
export DAILY_SUM_N2O_ATTR_FILEDESC="$(printf '%-60s' 'total N2O emissions for day by hour in mol/s')"

### monthly sum of N2O hourlies
# # export MONTHLY_SUM_N2O_DIR="${WORK_DIR}"
# export MONTHLY_SUM_N2O_DIR='/work/MOD3APP/rtd/emis/2008N2O/AQMEII-NA_N2O_integration/N2O' # terrae
export MONTHLY_SUM_N2O_FP_TEMPLATE_NCL="${DAILY_SUM_N2O_FP_TEMPLATE_NCL}"
## datavar
export MONTHLY_SUM_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export MONTHLY_SUM_N2O_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
## dimensions
export MONTHLY_SUM_N2O_DIM_VAR_N='1' # for IOAPI: N2O is sole datavar
# file/global attributes
export MONTHLY_SUM_N2O_ATTR_FILEDESC="$(printf '%-60s' 'total N2O emissions for month by hour in mol/s')"

### yearly sum of N2O hourlies
# # export YEARLY_SUM_N2O_DIR="${WORK_DIR}"
# export YEARLY_SUM_N2O_DIR='/work/MOD3APP/rtd/emis/2008N2O/AQMEII-NA_N2O_integration/N2O' # terrae
export YEARLY_SUM_N2O_FP_TEMPLATE_NCL="${DAILY_SUM_N2O_FP_TEMPLATE_NCL}"
## datavar
export YEARLY_SUM_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export YEARLY_SUM_N2O_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
## dimensions
export YEARLY_SUM_N2O_DIM_VAR_N='1' # for IOAPI: N2O is sole datavar
# file/global attributes
export YEARLY_SUM_N2O_ATTR_FILEDESC="$(printf '%-60s' 'total N2O emissions for year by hour in mol/s')"

# ----------------------------------------------------------------------
# final output: combined hourly emissions
# ----------------------------------------------------------------------

# TODO: do name substitutions in N2O_plus_AQ.ncl!
export DAILY_ALL_DIR="${ALL_SUM_DIR}" # terrae
export DAILY_ALL_FP_TEMPLATE_NCL="${ALL_SUM_FP_TEMPLATE_NCL}"
export DAILY_ALL_FP_TEMPLATE_CMAQ="${ALL_SUM_FP_TEMPLATE_CMAQ}"
## N2O datavar
export DAILY_ALL_N2O_DATAVAR_NAME="${AQMEIINA_DV_NAME}"

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup functions
# ----------------------------------------------------------------------

# # fails
# function command_not_found_handle {
#   CMD=$1
#   echo -e "\n$ ${THIS_FN}: command=${CMD} not found\n"
#   exit 2
# }

# # fails
# # see Tedzdog Jan 23 '12 at 6:11 @ http://unix.stackexchange.com/questions/29689/how-do-i-redefine-a-bash-function-in-terms-of-old-definition
# eval 'command_not_found_handle () {
#   CMD=$1
#   echo -e "\n$ ${THIS_FN}: command=${CMD} not found\n"
#   exit 3
# }'

# ----------------------------------------------------------------------
# setup functions: code
# ----------------------------------------------------------------------

### TODO: test for resources, reuse if available
### `setup_paths` isa bash util, so `get_helpers` first
### `setup_apps` ditto
function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    "mkdir -p ${WORK_DIR}" \
    'get_helpers' \
    'setup_paths' \
    'setup_apps' \
    'check_resources' \
  ; do
    echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 4
    fi
  done
#  echo -e "\n$ ${THIS_FN}: PDF_VIEWER='${PDF_VIEWER}'" # debugging
} # end function setup

### Get needed helper modules. Note 'get_regrid_utils' must go first, since latter rely on it.
#    'get_check_conservation' \
function get_helpers {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # may not need/want to get_kludge_spinup
  # or conversely get_sum_N2O+get_N2O_plus_AQ
  for CMD in \
    'get_regrid_utils' \
    'get_bash_utils' \
    'get_filepath_funcs' \
    'get_IOAPI_funcs' \
    'get_string_funcs' \
    'get_summarize' \
    'get_time_funcs' \
    'get_sum_N2O' \
    'get_N2O_plus_AQ' \
    'get_kludge_spinup' \
  ; do
    echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 5
    fi
  done
} # end function get_helpers

### This gets "package"=regrid_utils from which the remaining helpers are (at least potentially) drawn.
function get_regrid_utils {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} REGRID_UTILS_DIR not defined"
    exit 6
  fi

  # Noticed a problem where code committed to regrid_utils was not replacing code in existing ${REGRID_UTILS_DIR}, so ...
  if [[ -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${MESSAGE_PREFIX} removing existing regrid_utils folder='${REGRID_UTILS_DIR}'"
    for CMD in \
      "rm -fr ${REGRID_UTILS_DIR}/" \
     ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 7
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
  # assumes GIT_CLONE_PREFIX set in environment by, e.g., uber_driver
    for CMD in \
      "pushd ${WORK_DIR}" \
      "${GIT_CLONE_PREFIX} git clone ${REGRID_UTILS_URI}.git" \
      'popd' \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo # newline
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found"
        echo -e "\t(suggestion: check that GIT_CLONE_PREFIX is set appropriately in calling environment)"
        echo # newline
        exit 8
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot download regrid_utils to '${REGRID_UTILS_DIR}'"
    exit 9
  fi  
} # end function get_regrid_utils

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${BASH_UTILS_FP} before running this script.
function get_bash_utils {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${BASH_UTILS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BASH_UTILS_FP not defined"
    exit 10
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${BASH_UTILS_FN} ${BASH_UTILS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 11
      fi
    done
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BASH_UTILS_FP=='${BASH_UTILS_FP}' not readable"
    exit 12
  fi
  # This is bash, so gotta ...
  source "${BASH_UTILS_FP}"
  # ... for its functions to be available later in this script
} # end function get_bash_utils

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${FILEPATH_FUNCS_FP} before running this script.
function get_filepath_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${FILEPATH_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} FILEPATH_FUNCS_FP not defined"
    exit 13
  fi
  if [[ ! -r "${FILEPATH_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${FILEPATH_FUNCS_FN} ${FILEPATH_FUNCS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 14
      fi
    done
  fi
  if [[ ! -r "${FILEPATH_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} FILEPATH_FUNCS_FP=='${FILEPATH_FUNCS_FP}' not readable"
    exit 15
  fi
} # end function get_filepath_funcs

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${IOAPI_FUNCS_FP} before running this script.
function get_IOAPI_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: IOAPI_FUNCS_FP not defined"
    exit 16
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${IOAPI_FUNCS_FN} ${IOAPI_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} IOAPI_FUNCS_FP=='${IOAPI_FUNCS_FP}' not readable"
    exit 17
  fi
} # end function get_IOAPI_funcs

function get_N2O_plus_AQ {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${N2O_PLUS_AQ_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: N2O_PLUS_AQ_FP not defined"
    exit 18
  fi
  # is in this repo
#  if [[ ! -r "${N2O_PLUS_AQ_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${N2O_PLUS_AQ_FP} ${N2O_PLUS_AQ_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${N2O_PLUS_AQ_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} N2O_PLUS_AQ_FP=='${N2O_PLUS_AQ_FP}' not readable"
    exit 19
  fi
} # end function get_N2O_plus_AQ

function get_kludge_spinup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${KLUDGE_SPINUP_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: KLUDGE_SPINUP_FP not defined"
    exit 18
  fi
  # is in this repo
#  if [[ ! -r "${KLUDGE_SPINUP_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${KLUDGE_SPINUP_FP} ${N2O_PLUS_AQ_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${KLUDGE_SPINUP_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} KLUDGE_SPINUP_FP=='${KLUDGE_SPINUP_FP}' not readable"
    exit 19
  fi
} # end function get_kludge_spinup

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${R_UTILS_FP} before running this script.
function get_R_utils {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${R_UTILS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} R_UTILS_FP not defined"
    exit 20
  fi
  if [[ ! -r "${R_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${R_UTILS_FN} ${R_UTILS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 21
      fi
    done
  fi
  if [[ ! -r "${R_UTILS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} R_UTILS_FP=='${R_UTILS_FP}' not readable"
    exit 22
  fi
} # end function get_R_utils

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${STATS_FUNCS_FP} before running this script.
function get_stats_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} STATS_FUNCS_FP not defined"
    exit 23
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STATS_FUNCS_FN} ${STATS_FUNCS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 24
      fi
    done
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} STATS_FUNCS_FP=='${STATS_FUNCS_FP}' not readable"
    exit 25
  fi
} # end function get_stats_funcs

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${STRING_FUNCS_FP} before running this script.
function get_string_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} STRING_FUNCS_FP not defined"
    exit 26
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STRING_FUNCS_FN} ${STRING_FUNCS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 27
      fi
    done
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} STRING_FUNCS_FP=='${STRING_FUNCS_FP}' not readable"
    exit 28
  fi
} # end function get_string_funcs

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${TIME_FUNCS_FP} before running this script.
function get_time_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} TIME_FUNCS_FP not defined"
    exit 29
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${TIME_FUNCS_FN} ${TIME_FUNCS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 30
      fi
    done
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} TIME_FUNCS_FP=='${TIME_FUNCS_FP}' not readable"
    exit 31
  fi
} # end function get_time_funcs

function get_sum_N2O {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${SUM_N2O_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: SUM_N2O_FP not defined"
    exit 32
  fi
  # is in this repo
#  if [[ ! -r "${SUM_N2O_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${SUM_N2O_FP} ${SUM_N2O_URI}" \
#    ; do
#      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${SUM_N2O_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} SUM_N2O_FP=='${SUM_N2O_FP}' not readable"
    exit 33
  fi
} # end function get_sum_N2O

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${SUMMARIZE_FUNCS_FP} before running this script.
function get_summarize {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} SUMMARIZE_FUNCS_FP not defined"
    exit 34
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${SUMMARIZE_FUNCS_FN} ${SUMMARIZE_FUNCS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 35
      fi
    done
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} SUMMARIZE_FUNCS_FP=='${SUMMARIZE_FUNCS_FP}' not readable"
    exit 36
  fi
} # end function get_summarize

### isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${VIS_FUNCS_FP} before running this script.
function get_vis_funcs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} VIS_FUNCS_FP not defined"
    exit 37
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${VIS_FUNCS_FN} ${VIS_FUNCS_FP}" \
    ; do
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        exit 38
      fi
    done
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} VIS_FUNCS_FP=='${VIS_FUNCS_FP}' not readable"
    exit 39
  fi
} # end function get_vis_funcs

# ----------------------------------------------------------------------
# setup functions: resources
# ----------------------------------------------------------------------

### Check that ag-soil netCDF is where and how we say it is
function check_agsoil_hourly {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${AGSOIL_N2O_FP}" ]] ; then
    FOUND_ALL_AGSOIL_HOURLY=false
    echo -e "${ERROR_PREFIX} AGSOIL_N2O_FP not defined"
    exit 40
  fi
  if [[ ! -r "${AGSOIL_N2O_FP}" ]] ; then
#    echo -e "${ERROR_PREFIX} agsoil datasource='${AGSOIL_N2O_FP}' not readable"
#    exit 41
    echo -e "${MESSAGE_PREFIX} agsoil datasource='${AGSOIL_N2O_FP}' not readable"
    FOUND_ALL_AGSOIL_HOURLY=false
  else
    FOUND_ALL_AGSOIL_HOURLY=true
  fi
} # end function check_agsoil_hourly

### Check that CLM_CN netCDF are where and how we say they are
### TODO: refactor to bash_utilities.sh, reuse here and uber_driver::setup_natsoil
function check_CLM_hourly {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  local YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`
  if [[ -z "${NATSOIL_FP_TEMPLATE}" ]] ; then
    FOUND_ALL_CLM_HOURLY=false
    echo -e "${ERROR_PREFIX} NATSOIL_FP_TEMPLATE not defined"
    exit 42
  fi
  # count months--easy
  for (( M=1; M<=12; M++ )) ; do
    MM="$(printf '%02d' ${M})"
    YYYYMM="${YYYY}${MM}"
    # bash string replacement. TODO: document bash version dependence
    NATSOIL_N2O_FP="${NATSOIL_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMM}}"
    if [[ ! -r "${NATSOIL_N2O_FP}" ]] ; then
#      echo -e "${ERROR_PREFIX} natural-soil datasource='${NATSOIL_N2O_FP}' not readable"
#      exit 43
      echo -e "${MESSAGE_PREFIX} natural-soil datasource='${NATSOIL_N2O_FP}' not readable"
      FOUND_ALL_CLM_HOURLY=false
      break # for loop
    fi
  done # counting months
  FOUND_ALL_CLM_HOURLY=true
} # end function check_CLM_hourly

### Check that EDGAR netCDF is where and how we say it is
function check_EDGAR_hourly {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${NONSOIL_ANTHRO_N2O_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} NONSOIL_ANTHRO_N2O_FP not defined"
    exit 44
  fi
  if [[ ! -r "${NONSOIL_ANTHRO_N2O_FP}" ]] ; then
#    echo -e "${ERROR_PREFIX} NONSOIL_ANTHRO datasource='${NONSOIL_ANTHRO_N2O_FP}' not readable"
#    exit 45
    echo -e "${MESSAGE_PREFIX} NONSOIL_ANTHRO datasource='${NONSOIL_ANTHRO_N2O_FP}' not readable"
    FOUND_ALL_EDGAR_HOURLY=false
  else
    FOUND_ALL_EDGAR_HOURLY=true
  fi
} # end function check_EDGAR_hourly

### Check that GEIA netCDF is where and how we say it is
function check_GEIA_hourly {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${MARINE_N2O_FP}" ]] ; then
    FOUND_ALL_GEIA_HOURLY=false
    echo -e "${ERROR_PREFIX} MARINE_N2O_FP not defined"
    exit 46
  fi
  if [[ ! -r "${MARINE_N2O_FP}" ]] ; then
#    echo -e "${ERROR_PREFIX} marine datasource='${MARINE_N2O_FP}' not readable"
#    exit 47
    echo -e "${MESSAGE_PREFIX} marine datasource='${MARINE_N2O_FP}' not readable"
    FOUND_ALL_GEIA_HOURLY=false
  else
    FOUND_ALL_GEIA_HOURLY=true
  fi
} # end function check_GEIA_hourly

### Check that GFED netCDF are where and how we say they are
### TODO: refactor to bash_utilities.sh, reuse here, check_AQ_inputs and uber_driver::setup_burning
function check_GFED_hourly {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`
  if [[ -z "${BURNING_FP_TEMPLATE}" ]] ; then
    FOUND_ALL_GFED_HOURLY=false
    echo -e "${ERROR_PREFIX} BURNING_FP_TEMPLATE not defined"
    exit 48
  fi

  ### count months and days--not so easy
  local DD='%d'           # output "<2-digit day/>"
  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  local TZ_BAK="${TZ}"
  export TZ='UTC0'
  local YYYY="$(printf '%04d' ${MODEL_YEAR})" # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
    MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

    FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
    FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#     echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

    ## note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
    LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#     echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

    for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
      YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
      # bash string replacement. TODO: document bash version dependence
      BURNING_N2O_FP="${BURNING_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      if [[ ! -r "${BURNING_N2O_FP}" ]] ; then
#        echo -e "${ERROR_PREFIX} biomass-burning datasource='${BURNING_N2O_FP}' not readable"
#        exit 49
        echo -e "${MESSAGE_PREFIX} biomass-burning datasource='${BURNING_N2O_FP}' not readable"
        FOUND_ALL_GFED_HOURLY=false
        break # inner for loop. TODO: break out of function!
      fi
    done # iterate days
  done # iterate months
  export TZ="${TZ_BAK}" # restore value
  FOUND_ALL_GFED_HOURLY=true
} # end function check_GFED_hourly

### Note structural similarity to check_final_outputs. TODO: refactor.
function check_N2O_inputs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Note this is reverse logic of function=check_final_outputs ('&&' not '||').
  ### Set (true) all flags prospectively, break out of function (loop anyway) if any clear.
  FOUND_ALL_AGSOIL_HOURLY=true
  FOUND_ALL_CLM_HOURLY=true
  FOUND_ALL_EDGAR_HOURLY=true
  FOUND_ALL_GEIA_HOURLY=true
  FOUND_ALL_GFED_HOURLY=true
  FOUND_ALL_N2O_INPUTS=true

  for CMD in \
    'check_agsoil_hourly' \
    'check_CLM_hourly' \
    'check_EDGAR_hourly' \
    'check_GEIA_hourly' \
    'check_GFED_hourly' \
  ; do
    # Note syntax for testing booleans == executables (e.g., /bin/true , /bin/false)
    if ${FOUND_ALL_AGSOIL_HOURLY} && ${FOUND_ALL_CLM_HOURLY} && ${FOUND_ALL_EDGAR_HOURLY} && ${FOUND_ALL_GEIA_HOURLY} && ${FOUND_ALL_GFED_HOURLY} ; then
      ## all previously-called check_* functions succeeded
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        FOUND_ALL_N2O_INPUTS=false
        exit 50
      fi # if [[ $? -ne 0 ]]
    else
      ## most-previously-called check_* function failed
      FOUND_ALL_N2O_INPUTS=false
      break # out of loop. TODO: break out of function!
    fi # check union of inputs
  done

} # end function check_N2O_inputs

### Check that AQ netCDF (from previous PHASE/CDC run) are where and how we say they are
### TODO: refactor to bash_utilities.sh, reuse here and check_GFED_hourly
function check_AQ_inputs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${AQ_FP_TEMPLATE}" ]] ; then
    echo -e "${ERROR_PREFIX} AQ_FP_TEMPLATE not defined"
    FOUND_ALL_AQ_INPUTS=false
    exit 51
  fi

  ### Function "return value" set here, since I don't know how to "break out of function" on failure. (TODO: learn how!)
  ### Therefore, assume it's true, unless set false.
  FOUND_ALL_AQ_OUTPUTS=true

  ### count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"
  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
    if ! ${FOUND_ALL_FINAL_OUTPUTS} ; then
      break # outer for loop
    else
      MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

      FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
      FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#       echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

      ## note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
      LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#       echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

      for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
        YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
        # bash string replacement. TODO: document bash version dependence
        AQ_FP="${AQ_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
        if [[ ! -r "${AQ_FP}" ]] ; then
          # TODO: unzip from originals on /asm/ROMO/cdc/2008cdc/smoke_out/: see code in logfile.130715
#           echo -e "${ERROR_PREFIX} non-N2O-AQ datasource='${AQ_FP}' not readable"
#           exit 52
          echo -e "${MESSAGE_PREFIX} non-N2O-AQ datasource='${AQ_FP}' not readable"
          FOUND_ALL_AQ_INPUTS=false
          break # inner for loop. TODO: break out of function!
        fi
      done # iterate days
    fi # if ! ${FOUND_ALL_AQ_OUTPUTS}
  done # iterate months
  export TZ="${TZ_BAK}" # restore value
  FOUND_ALL_AQ_INPUTS=true

} # end function check_AQ_inputs

### Check that daily N2O netCDF (integration of subproject emissions) are where and how we say they are.
### TODO: refactor to bash_utilities.sh, reuse here and check_*_outputs*
function check_N2O_outputs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${DAILY_SUM_N2O_FP_TEMPLATE_NCL}" ]] ; then
    echo -e "${ERROR_PREFIX} DAILY_SUM_N2O_FP_TEMPLATE_NCL not defined"
    FOUND_ALL_N2O_OUTPUTS=false
    exit 53
  fi

  ### Function "return value" set here, since I don't know how to "break out of function" on failure. (TODO: learn how!)
  ### Therefore, assume it's true, unless set false.
  FOUND_ALL_N2O_OUTPUTS=true

  ### count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"
  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
    if ! ${FOUND_ALL_N2O_OUTPUTS} ; then
      break # outer for loop
    else
      MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

      FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
      FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#       echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

      ## note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
      LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#       echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

      for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
        YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
        # bash string replacement. TODO: document bash version dependence
        DAILY_SUM_N2O_FP="${DAILY_SUM_N2O_FP_TEMPLATE_NCL/${TEMPLATE_STRING}/${YYYYMMDD}}"
        if [[ ! -r "${DAILY_SUM_N2O_FP}" ]] ; then
#          echo -e "${ERROR_PREFIX} daily N2O-sum datasource='${DAILY_SUM_N2O_FP}' not readable"
#          exit 54
          echo -e "${MESSAGE_PREFIX} daily N2O-sum datasource='${DAILY_SUM_N2O_FP}' not readable"
          FOUND_ALL_N2O_OUTPUTS=false
        fi
      done # iterate days
    fi # if ! ${FOUND_ALL_N2O_OUTPUTS}
  done # iterate months
  export TZ="${TZ_BAK}" # restore value

} # end function check_N2O_outputs

### Check that daily output netCDF (integration of N2O and AQ emissions) are where and how we say they are:
### which might be either as *.nc or *.ncf (<sigh/>)
### TODO: refactor to bash_utilities.sh, reuse here and check_*_outputs*
function check_final_outputs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Note this is reverse logic of function=check_N2O_inputs ('||' not '&&').
  ### Clear all flags prospectively, break out of function (loop anyway) if any set (true).
  FOUND_ALL_FINAL_OUTPUTS=false
  FOUND_ALL_FINAL_OUTPUTS_CMAQ=false
  FOUND_ALL_FINAL_OUTPUTS_NCL=false

  for CMD in \
    'check_final_outputs_CMAQ' \
    'check_final_outputs_NCL' \
  ; do
    # Note syntax for testing booleans == executables (e.g., /bin/true , /bin/false)
    if ${FOUND_ALL_FINAL_OUTPUTS_CMAQ} || ${FOUND_ALL_FINAL_OUTPUTS_NCL} ; then
      ## most-previously-called check_* function failed
      FOUND_ALL_FINAL_OUTPUTS=true
      break # out of loop. TODO: break out of function!
    else
      ## all previously-called check_* functions failed
      echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
        FOUND_ALL_FINAL_OUTPUTS=false
        exit 55
      fi # if [[ $? -ne 0 ]]
    fi # check union of outputs
  done

} # end function check_final_outputs

### Check for daily output netCDF as *.ncf
### TODO: refactor to bash_utilities.sh, reuse here and check_final_outputs_NCL
function check_final_outputs_CMAQ {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${DAILY_ALL_FP_TEMPLATE_CMAQ}" ]] ; then
    echo -e "${ERROR_PREFIX} DAILY_ALL_FP_TEMPLATE_CMAQ not defined"
    FOUND_ALL_FINAL_OUTPUTS_CMAQ=false
    exit 56
  fi
#  echo -e "${MESSAGE_PREFIX} DAILY_ALL_FP_TEMPLATE_CMAQ='${DAILY_ALL_FP_TEMPLATE_CMAQ}'" # debugging

  ### Function "return value" set here, since I don't know how to "break out of function" on failure. (TODO: learn how!)
  ### Therefore, assume it's true, unless set false.
  FOUND_ALL_FINAL_OUTPUTS_CMAQ=true

  ### count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"
  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
#    if [[ ! "${FOUND_ALL_FINAL_OUTPUTS_CMAQ}" ]] ; then # incorrect syntax!
    if ! ${FOUND_ALL_FINAL_OUTPUTS_CMAQ} ; then
      break # outer for loop
    else
      MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

      FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
      FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#       echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

      ## note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
      LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#       echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

      for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
        YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
        # bash string replacement. TODO: document bash version dependence
        DAILY_ALL_FP="${DAILY_ALL_FP_TEMPLATE_CMAQ/${TEMPLATE_STRING}/${YYYYMMDD}}"
        if [[ ! -r "${DAILY_ALL_FP}" ]] ; then
#          echo -e "${ERROR_PREFIX} daily final output (N2O+AQ)='${DAILY_ALL_FP}' not readable"
#          exit 57
          echo -e "${MESSAGE_PREFIX} daily final output (N2O+AQ)='${DAILY_ALL_FP}' not readable"
          FOUND_ALL_FINAL_OUTPUTS_CMAQ=false
          break # inner for loop. TODO: break out of function!
        fi
      done # iterate days
    fi # if ! ${FOUND_ALL_FINAL_OUTPUTS_CMAQ}
  done # iterate months
  export TZ="${TZ_BAK}" # restore value

  if ${FOUND_ALL_FINAL_OUTPUTS_CMAQ} ; then
    echo -e "${MESSAGE_PREFIX} found all daily final output (N2O+AQ) as '*.${NETCDF_EXT_CMAQ}' in dir='${ALL_SUM_DIR}'"
  fi
} # end function check_final_outputs_CMAQ

### Check for daily output netCDF as *.nc
### TODO: refactor to bash_utilities.sh, reuse here and check_final_outputs_CMAQ
function check_final_outputs_NCL {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${DAILY_ALL_FP_TEMPLATE_NCL}" ]] ; then
    echo -e "${ERROR_PREFIX} DAILY_ALL_FP_TEMPLATE_NCL not defined"
    FOUND_ALL_FINAL_OUTPUTS_NCL=false
    exit 58
  fi
#  echo -e "${MESSAGE_PREFIX} DAILY_ALL_FP_TEMPLATE_NCL='${DAILY_ALL_FP_TEMPLATE_NCL}'" # debugging

  ### Function "return value" set here, since I don't know how to "break out of function" on failure. (TODO: learn how!)
  ### Therefore, assume it's true, unless set false.
  FOUND_ALL_FINAL_OUTPUTS_NCL=true

  ### count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"
  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
#    if [[ ! "${FOUND_ALL_FINAL_OUTPUTS_NCL}" ]] ; then # incorrect syntax!
    if ! ${FOUND_ALL_FINAL_OUTPUTS_NCL} ; then
      break # outer for loop
    else
      MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

      FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
      FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#       echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

      ## note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
      LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#       echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

      for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
        YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
        # bash string replacement. TODO: document bash version dependence
        DAILY_ALL_FP="${DAILY_ALL_FP_TEMPLATE_NCL/${TEMPLATE_STRING}/${YYYYMMDD}}"
        if [[ ! -r "${DAILY_ALL_FP}" ]] ; then
#          echo -e "${ERROR_PREFIX} daily final output (N2O+AQ)='${DAILY_ALL_FP}' not readable"
#          exit 59
          echo -e "${MESSAGE_PREFIX} daily final output (N2O+AQ)='${DAILY_ALL_FP}' not readable"
          FOUND_ALL_FINAL_OUTPUTS_NCL=false
          break # inner for loop. TODO: break out of function!
        fi
      done # iterate days
    fi # if ! ${FOUND_ALL_FINAL_OUTPUTS_NCL}
  done # iterate months
  export TZ="${TZ_BAK}" # restore value

  if ${FOUND_ALL_FINAL_OUTPUTS_NCL} ; then
    echo -e "${MESSAGE_PREFIX} found all daily final output (N2O+AQ) as '*.${NETCDF_EXT_NCL}' in dir='${ALL_SUM_DIR}'"
  fi
} # end function check_final_outputs_NCL

function check_resources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'check_N2O_inputs' \
    'check_N2O_outputs' \
    'check_AQ_inputs' \
    'check_final_outputs' \
  ; do
# TODO: check me!
    echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 60
    fi
  done
} # end function check_resources

# ----------------------------------------------------------------------
# action functions
# ----------------------------------------------------------------------

### Sum N2O emissions from subprojects.
### Depends on function get_sum_N2O above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function sum_N2O {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#   ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${SUM_N2O_FP}" \
  ; do
    cat <<EOM

About to run command="${CMD}"

EOM
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 61
    fi
  done

} # end function sum_N2O

### Combine previously-summed N2O emissions with previously-run AQ-only emissions.
### Depends on function get_N2O_plus_AQ above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function N2O_plus_AQ {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#   ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${N2O_PLUS_AQ_FP}" \
  ; do
    cat <<EOM

About to run command="${CMD}"

EOM
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 62
    fi
  done

} # end function N2O_plus_AQ

# ----------------------------------------------------------------------
# payload: building the integrated emissions (or not)
# ----------------------------------------------------------------------

function payload {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#   # start debugging
#   echo -e "${MESSAGE_PREFIX} FOUND_ALL_N2O_INPUTS='${FOUND_ALL_N2O_INPUTS}'"
#   echo -e "${MESSAGE_PREFIX} FOUND_ALL_N2O_OUTPUTS='${FOUND_ALL_N2O_OUTPUTS}'"
#   echo -e "${MESSAGE_PREFIX} FOUND_ALL_AQ_INPUTS='${FOUND_ALL_AQ_INPUTS}'"
#   echo -e "${MESSAGE_PREFIX} FOUND_ALL_FINAL_OUTPUTS='${FOUND_ALL_FINAL_OUTPUTS}'"
#   #   end debugging

  if ${FOUND_ALL_FINAL_OUTPUTS} ; then
    echo -e "${MESSAGE_PREFIX} Found final outputs, move or delete to rebuild. Exiting ..."
    exit 64
  else
    if ${FOUND_ALL_AQ_INPUTS} ; then
      if ${FOUND_ALL_N2O_OUTPUTS} ; then
        # ${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_AQ_INPUTS}
        echo -e "${MESSAGE_PREFIX} Can/will build final outputs."
 
       for CMD in \
          'N2O_plus_AQ' \
          'check_final_outputs' \
        ; do
          echo -e "\n${MESSAGE_PREFIX} '${CMD}'\n"
          eval "${CMD}" # comment this out for NOPing, e.g., to `source`
          if [[ $? -ne 0 ]] ; then
            echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
            exit 65
          fi
        done

        if ! ${FOUND_ALL_FINAL_OUTPUTS} ; then
          echo -e "${ERROR_PREFIX} Failed to build final outputs, exiting ..."
          exit 66
        fi
        # else
        exit 0 # success!

      else
        # !${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_AQ_INPUTS}
        if ${FOUND_ALL_N2O_INPUTS} ; then
          # ${FOUND_ALL_N2O_INPUTS} && !${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_AQ_INPUTS}
          echo -e "${MESSAGE_PREFIX} Can/will build N2O outputs."

          for CMD in \
            'sum_N2O' \
            'check_N2O_outputs' \
          ; do
            echo -e "\n${MESSAGE_PREFIX} '${CMD}'\n"
            eval "${CMD}" # comment this out for NOPing, e.g., to `source`
            if [[ $? -ne 0 ]] ; then
              echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
              exit 67
            fi
          done

          if ${FOUND_ALL_N2O_OUTPUTS} ; then # TODO: recurse
            # ${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_AQ_INPUTS}
            echo -e "${MESSAGE_PREFIX} Can/will build final outputs."

            for CMD in \
              'N2O_plus_AQ' \
              'check_final_outputs' \
            ; do
              echo -e "\n${MESSAGE_PREFIX} '${CMD}'\n"
              eval "${CMD}" # comment this out for NOPing, e.g., to `source`
              if [[ $? -ne 0 ]] ; then
                echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
                exit 68
              fi
            done

            if ! ${FOUND_ALL_FINAL_OUTPUTS} ; then
              echo -e "${ERROR_PREFIX} Failed to build final outputs, exiting ..."
              exit 69
            fi
            # else
            exit 0 # success!
          else
            # !${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_AQ_INPUTS}
            echo -e "${ERROR_PREFIX} Failed to build N2O outputs, thus cannot build final outputs, exiting ..."
            exit 70
          fi
        else
          # !${FOUND_ALL_N2O_INPUTS} && !${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_AQ_INPUTS}
          echo -e "${ERROR_PREFIX} Cannot find N2O inputs or outputs, thus cannot build final outputs, exiting ..."
          exit 71
        fi # if ${FOUND_ALL_N2O_INPUTS}
      fi # if ${FOUND_ALL_N2O_OUTPUTS}
    else
      # !${FOUND_ALL_FINAL_OUTPUTS} && !${FOUND_ALL_AQ_INPUTS}
      echo -e "${ERROR_PREFIX} Cannot find AQ inputs, thus cannot build final outputs, exiting ..."
      if ${FOUND_ALL_N2O_INPUTS} && ! ${FOUND_ALL_N2O_OUTPUTS} ; then
        # !${FOUND_ALL_FINAL_OUTPUTS} && !${FOUND_ALL_AQ_INPUTS} && !${FOUND_ALL_N2O_OUTPUTS} && ${FOUND_ALL_N2O_INPUTSfi
        echo -e "${MESSAGE_PREFIX} Can/will build N2O outputs."

        for CMD in \
          'sum_N2O' \
          'check_N2O_outputs' \
        ; do
          echo -e "\n${MESSAGE_PREFIX} '${CMD}'\n"
          eval "${CMD}" # comment this out for NOPing, e.g., to `source`
          if [[ $? -ne 0 ]] ; then
            echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
            exit 72
          fi
        done

        if ! ${FOUND_ALL_N2O_OUTPUTS} ; then
          echo -e "${ERROR_PREFIX} Failed to build N2O outputs, exiting ..."
          exit 73
        fi
      else
        echo -e "${ERROR_PREFIX} Cannot find N2O inputs, thus cannot build N2O outputs), exiting ..."
        exit 74
      fi # if ${FOUND_ALL_N2O_INPUTS} && ! ${FOUND_ALL_N2O_OUTPUTS}
    fi # if ${FOUND_ALL_AQ_INPUTS}"
  fi # if ${FOUND_ALL_FINAL_OUTPUTS}"

} # end function payload

# ----------------------------------------------------------------------
# kludge_spinup: kludge the integrated emissions for the spinup period (or not)
# ----------------------------------------------------------------------

function kludge_spinup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Unfortunately hasta do its own testing for presence of inputs, outputs.
  ### TODO: find a way to reuse above code for setting FOUND_ALL_*_*PUTS

#   ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${KLUDGE_SPINUP_FP}" \
  ; do
    cat <<EOM

About to run command="${CMD}"

EOM
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 62
    fi
  done

} # end function kludge_spinup

# ----------------------------------------------------------------------
# teardown: currently kludge does its own
# ----------------------------------------------------------------------

function teardown {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    "ls -alt ${N2O_SUM_TARGET_FP_TEMPLATE/${TEMPLATE_STRING}/????????} | head" \
    "ls -alt ${DAILY_ALL_FP_TEMPLATE_CMAQ/${TEMPLATE_STRING}/????????} | head" \
    "ls -alt ${WORK_DIR}/*.pdf | head" \
  ; do
    echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
      exit 63
    fi
  done
} # end function teardown

# ----------------------------------------------------------------------
# main
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#   'setup' \           # paths, helpers, etc
#   'payload' \         # create integrated emissions (and N2O sum if necessary and) if feasible
#   'kludge_spinup' \   # copy spinup emissions end 2008 -> end 2007
#   'teardown' \        # not much right now, eventually should tidy and test (e.g., plot display)
# should always
# * begin with `setup` to do `module add`
# * end with `teardown`
for CMD in \
  'setup' \
  'kludge_spinup' \
  'teardown' \
; do
  echo -e "\n${MESSAGE_PREFIX} '${CMD}'\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "\n${ERROR_PREFIX} '${CMD}': failed or not found\n"
    exit 75
  fi
done

# ----------------------------------------------------------------------
# debugging
# ----------------------------------------------------------------------
