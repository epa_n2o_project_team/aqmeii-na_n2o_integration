#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# This script clones and runs code either from
# * the `git` repository @
# https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration
#   You probably wanna check its README before running this.

# * the current folder (for code you haven't committed).

# If you decide to run this, you should first

# * un/comment var=ACCESS to reflect your available/desired file-transfer protocol
# * if using ACCESS='file', edit FILE_LIST below
# * edit var=REPO_ROOT to state where to create your repo clone
# * remember to enable netCDF-4 on your system, e.g.
# > module add netcdf-4.1.2 # on terrae (a cluster on which I work)

# and run this like
# START="$(date)" ; echo -e "START=${START}" ; rm -fr /tmp/aqmeii-na_n2o_integration/ ; ./uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

# Note: this script requires
# * `date`
# * `bash` version recent enough for string-replacement syntax

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS=$0
THIS_FN="$(basename ${THIS})"

REPO_ROOT='/tmp' # or location of your choice: repo/workspace created in subdir
PROJECT_NAME='aqmeii-na_n2o_integration' # TODO: get from commandline
REPO_DIR="${REPO_ROOT}/${PROJECT_NAME}"
# what actually does the work
DRIVER_FN='N2O_integration_driver.sh'    # TODO: get from commandline

### following are "booleans": comment out or change value if !TRUE
# # Does our platform allow git/ssh? (e.g., terrae does not)
# CAN_GIT_SSH='TRUE'
# # Does our platform have HTTP certificates? (e.g., terrae does not)
# HAVE_HTTP_CERTS='TRUE'
# if latter is false, git must not check for certificates
if [[ -z "${HAVE_HTTP_CERTS}" || "${HAVE_HTTP_CERTS}" != 'TRUE' ]] ; then
  # set for ${DRIVER_FN}
  export GIT_CLONE_PREFIX='env GIT_SSL_NO_VERIFY=true'
else
  export GIT_CLONE_PREFIX=''
fi

### modes of access to sources (see setup_sources): either copy files or get from repo
ACCESS='file'       # copy code in current directory
# ACCESS=='file' uses no repository
# ACCESS='git/ssh'    # outgoing ssh works (preferred if available)
# REPO_URI_GIT="git@bitbucket.org:tlroche/${PROJECT_NAME}.git"
# ACCESS='http+cert'  # outgoing ssh blocked, but you have certificates
# REPO_URI_HTTP="https://tlroche@bitbucket.org/tlroche/${PROJECT_NAME}.git"
# ACCESS='http-cert'  # (terrae) outgoing ssh blocked, no certs installed
# also uses REPO_URI_HTTP

# if [[ "${ACCESS}" == 'file' ]], you'll need
FILE_LIST="./${DRIVER_FN} \
           ./N2O_plus_AQ.ncl \
           ./resource_strings.sh \
           ./sum_N2O.ncl "
# TODO: get members(${FILE_LIST}) from ${DRIVER_FN}

### needed resources (see setup_resources)
source ./resource_strings.sh # allow reuse in N2O_integration_driver.sh

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function setup_sources {
  if [[ "${ACCESS}" == 'file' ]] ; then
    if [[ ! -x "./${DRIVER_FN}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: driver=='${DRIVER_FN}' not executable (with ACCESS==${ACCESS})"
      exit 1
    else
      for CMD in \
        "mkdir -p ${REPO_DIR}" \
        "cp ${FILE_LIST} ${REPO_DIR}/ " \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
            echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
            exit 2
        fi
      done
    fi

  else # ACCESS != 'file', i.e., from repo

    if   [[ "${ACCESS}" == 'git/ssh' ]] ; then
      if [[ -n "${CAN_GIT_SSH}" && "${CAN_GIT_SSH}" == 'TRUE' ]] ; then
        REPO_URI="${REPO_URI_GIT}"
      else
        echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot use access==git/ssh on this platform\n"
        exit 3
      fi
    elif [[ "${ACCESS}" == 'http+cert' ]] ; then
      if [[ -n "${HAVE_HTTP_CERTS}" && "${HAVE_HTTP_CERTS}" == 'TRUE' ]] ; then
        REPO_URI="${REPO_URI_HTTP}"
      else
        echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot use access==http+cert on this platform\n"
        exit 4
      fi
    elif [[ "${ACCESS}" == 'http-cert' ]] ; then
      REPO_URI="${REPO_URI_HTTP}"
    fi

    for CMD in \
      "pushd ${REPO_ROOT}" \
      "${GIT_CLONE_PREFIX} git clone ${REPO_URI}" \
      "popd" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
          echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
          exit 5
      fi
    done

  fi # end testing ACCESS
} # end function setup_sources

function setup_resources {
#   echo -e "${THIS_FN}::${FUNCNAME[0]}: gotta symlink to datasources from runspace==${REPO_DIR}"

  # comment out 'setup_N2O_sums' to re/build your own
  for CMD in \
    "pushd ${REPO_DIR}" \
    'setup_agsoil' \
    'setup_burning' \
    'setup_marine' \
    'setup_natsoil' \
    'setup_nonsoil_anthro' \
    'setup_N2O_sums' \
    'setup_AQ' \
    "popd" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
      exit 6
    fi
  done
} # end function setup_resources

function setup_agsoil {
  for CMD in \
   "mkdir -p $(dirname ${AGSOIL_TARGET_FP})" \
   "ln -s ${AGSOIL_SOURCE_FP} ${AGSOIL_TARGET_FP}" \
  ; do
#     echo -e "$ ${CMD}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
      exit 7
    fi
  done
} # end function setup_agsoil

### Preposition/stage GFED netCDF as needed.
### TODO: refactor to bash_utilities.sh, reuse for ::setup_{AQ, burning,N2O_sums} and in driver::check_*
function setup_burning {
  # count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"

  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
    MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

    FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
    FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#     echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

    # note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
    LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#     echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

    for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
      YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
      # bash string replacement. TODO: document bash version dependence
      BURNING_SOURCE_FN="${BURNING_SOURCE_FN_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      BURNING_SOURCE_FP="${BURNING_SOURCE_DIR}/${BURNING_SOURCE_FN}"
      BURNING_TARGET_FN="${BURNING_TARGET_FN_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      BURNING_TARGET_FP="${BURNING_TARGET_DIR}/${BURNING_TARGET_FN}"
      for CMD in \
        "mkdir -p $(dirname ${BURNING_TARGET_FP})" \
        "ln -s ${BURNING_SOURCE_FP} ${BURNING_TARGET_FP}" \
      ; do
#         echo -e "$ ${CMD}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
          exit 8
        fi
      done
    done # iterate days
  done # iterate months
  export TZ="${TZ_BAK}" # restore value
} # end function setup_burning

function setup_marine {
  for CMD in \
    "mkdir -p $(dirname ${MARINE_TARGET_FP})" \
    "ln -s ${MARINE_SOURCE_FP} ${MARINE_TARGET_FP}" \
  ; do
#     echo -e "$ ${CMD}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
      exit 9
    fi
  done
} # end function setup_marine

# TODO: refactor to bash_utilities.sh, reuse here and driver::check_natsoil_hourly
function setup_natsoil {
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`
  # count months--easy
  for (( M=1; M<=12; M++ )) ; do
    MM="$(printf '%02d' ${M})"
    YYYYMM="${YYYY}${MM}"
    # bash string replacement. TODO: document bash version dependence
    NATSOIL_SOURCE_FN="${NATSOIL_SOURCE_FN_TEMPLATE/${TEMPLATE_STRING}/${YYYYMM}}"
    NATSOIL_SOURCE_FP="${NATSOIL_SOURCE_DIR}/${NATSOIL_SOURCE_FN}"
    NATSOIL_TARGET_FN="${NATSOIL_TARGET_FN_TEMPLATE/${TEMPLATE_STRING}/${YYYYMM}}"
    NATSOIL_TARGET_FP="${NATSOIL_TARGET_DIR}/${NATSOIL_TARGET_FN}"
    for CMD in \
      "mkdir -p $(dirname ${NATSOIL_TARGET_FP})" \
      "ln -s ${NATSOIL_SOURCE_FP} ${NATSOIL_TARGET_FP}" \
    ; do
#       echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
        exit 10
      fi
    done
  done # counting
} # end function setup_natsoil

function setup_nonsoil_anthro {
  for CMD in \
    "mkdir -p $(dirname ${NONSOIL_ANTHRO_TARGET_FP})" \
    "ln -s ${NONSOIL_ANTHRO_SOURCE_FP} ${NONSOIL_ANTHRO_TARGET_FP}" \
  ; do
#     echo -e "$ ${CMD}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
      exit 11
    fi
  done
} # end function setup_nonsoil_anthro

### Preposition/stage N2O sums if desired (i.e., unless you want to rebuild them).
### TODO: refactor to bash_utilities.sh, reuse for ::setup_{AQ, burning,N2O_sums} and in driver::check_*
function setup_N2O_sums {
  # count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"

  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`
  # bash string replacement. TODO: document bash version dependence
  YEARLY_SUM_SOURCE_FP="${N2O_SUM_SOURCE_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYY}}"
  YEARLY_SUM_TARGET_FP="${N2O_SUM_TARGET_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYY}}"
  if [[ ! -r "${YEARLY_SUM_TARGET_FP}" ]] ; then
    for CMD in \
      "mkdir -p $(dirname ${YEARLY_SUM_TARGET_FP})" \
      "ln -s ${YEARLY_SUM_SOURCE_FP} ${YEARLY_SUM_TARGET_FP}" \
      ; do
#       echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed to setup yearlies (with ACCESS==${ACCESS})\n"
        exit 12
      fi
    done
  fi # [[ ! -r "${YEARLY_SUM_TARGET_FP}" ]]

  for (( M=1; M<=12; M++ )) ; do  # iterate months
    MM="$(printf '%02d' ${M})"    # 2-digit month for `date`
    YYYYMM="${YYYY}${MM}"
    # bash string replacement
    MONTHLY_SUM_SOURCE_FP="${N2O_SUM_SOURCE_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMM}}"
    MONTHLY_SUM_TARGET_FP="${N2O_SUM_TARGET_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMM}}"
    if [[ ! -r "${MONTHLY_SUM_TARGET_FP}" ]] ; then
      for CMD in \
        "mkdir -p $(dirname ${MONTHLY_SUM_TARGET_FP})" \
        "ln -s ${MONTHLY_SUM_SOURCE_FP} ${MONTHLY_SUM_TARGET_FP}" \
      ; do
#         echo -e "$ ${CMD}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed to setup monthlies (with ACCESS==${ACCESS})\n"
          exit 13
        fi
      done
    fi # [[ ! -r "${MONTHLY_SUM_TARGET_FP}" ]]

    FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
    FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#     echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

    # note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
    LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#     echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

    for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
      YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
      # bash string replacement
      DAILY_SUM_SOURCE_FP="${N2O_SUM_SOURCE_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      DAILY_SUM_TARGET_FP="${N2O_SUM_TARGET_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      if [[ ! -r "${DAILY_SUM_TARGET_FP}" ]] ; then
        for CMD in \
          "mkdir -p $(dirname ${DAILY_SUM_TARGET_FP})" \
          "ln -s ${DAILY_SUM_SOURCE_FP} ${DAILY_SUM_TARGET_FP}" \
        ; do
#           echo -e "$ ${CMD}"
          eval "${CMD}"
          if [[ $? -ne 0 ]] ; then
            echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed to setup dailies (with ACCESS==${ACCESS})\n"
            exit 14
          fi
        done
      fi # [[ ! -r "${DAILY_SUM_TARGET_FP}" ]]
    done # iterate days
  done # iterate months
  export TZ="${TZ_BAK}" # restore value
} # end function setup_N2O_sums

### Preposition/stage AQ netCDF input to previous PHASE/CDC run(s): can't rebuild them (with my code, anyway)
### TODO: refactor to bash_utilities.sh, reuse for ::setup_{AQ, burning,N2O_sums} and in driver::check_*
function setup_AQ {
  # count months and days--not so easy
  DD='%d'               # output "<2-digit day/>"

  # regarding `TZ='UTC0'` see end of http://lists.gnu.org/archive/html/bug-coreutils/2012-12/msg00003.html
  # setting TZ here rather than inline because I want to output with DD
  TZ_BAK="${TZ}"
  export TZ='UTC0'
  YYYY="$(printf '%04d' ${MODEL_YEAR})"    # 4-digit year for `date`

  for (( M=1; M<=12; M++ )) ; do  # iterate months
    MM="$(printf '%02d' ${M})"    # 2-digit month for `date`

    FIRST_DAY_OF_MONTH="${YYYY}${MM}01"
    FIRST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH}" +${DD})"
#     echo -e "first day of month=${FIRST_DAY_OF_MONTH_STRING}" # debugging

    # note: this will fail oddly (bad March and November!) without `TZ='UTC0'`, on both tlrPanP5 and terrae
    LAST_DAY_OF_MONTH_STRING="$(date -d "${FIRST_DAY_OF_MONTH} + 1 month - 1 day" +${DD})"
#     echo -e " last day of month=${LAST_DAY_OF_MONTH_STRING}" # debugging

    for (( D=1; D<=${LAST_DAY_OF_MONTH_STRING}; D++ )) ; do  # iterate days
      YYYYMMDD="${YYYY}${MM}$(printf '%02d' ${D})"
      # bash string replacement. TODO: document bash version dependence
      AQ_SOURCE_FP="${AQ_SOURCE_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      AQ_TARGET_FP="${AQ_TARGET_FP_TEMPLATE/${TEMPLATE_STRING}/${YYYYMMDD}}"
      for CMD in \
        "mkdir -p $(dirname ${AQ_TARGET_FP})" \
        "ln -s ${AQ_SOURCE_FP} ${AQ_TARGET_FP}" \
      ; do
#         echo -e "$ ${CMD}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed to setup AQ dailies (with ACCESS==${ACCESS})\n"
          exit 15
        fi
      done
    done # iterate days
  done # iterate months
  export TZ="${TZ_BAK}" # restore value
} # end function setup_AQ

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

if [[ -z "${REPO_DIR}" ]] ; then
  echo -e "${THIS_FN}:ERROR: REPO_DIR not defined"
  exit 16
fi

if [[ -d "${REPO_DIR}" ]] ; then
  echo -e "${THIS_FN}:ERROR? repo dir='${REPO_DIR}' exists: move or delete it before running this script"
  exit 17
fi

if [[ -z "${ACCESS}" ]] ; then
  echo -e "${THIS_FN}: ERROR: ACCESS not defined: did you uncomment one of the choices above?"
  exit 18
fi

for CMD in \
  'setup_sources' \
  'setup_resources' \
  "pushd ${REPO_DIR}" \
  "./${DRIVER_FN}" \
; do
#   "ls -alhR ${REPO_DIR}" \
  echo -e "\n$ ${THIS_FN}::main loop::${CMD}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "${THIS_FN}::main loop::${CMD}: ERROR: failed or not found\n"
    exit 19
  fi
done
