### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Envvars used in both N2O_integration_driver.sh and uber_driver.sh

### needed resources (see setup_resources)
# export WORK_DIR='.'
export WORK_DIR="${HOME}/code/regridding/AQMEII-NA_N2O_integration"
# export RESOURCE_ROOT='..' # nope! relative to ${WORK_DIR}
export RESOURCE_ROOT="${HOME}/code/regridding"
export MODEL_YEAR='2008'
## canonical extensions
export NETCDF_EXT_NCL='nc'   # NCL prefers the "canonical" netCDF extension
export NETCDF_EXT_CMAQ='ncf' # CMAQ, as is so often the case, is nonstandard :-(

### one template to bind them all ...
export TEMPLATE_STRING='@@@@@@@@' # YYYYMMDD

## annual resources (one file/year)

export AGSOIL_PROJECT_NAME='AQMEII_ag_soil'
export AGSOIL_SOURCE_FN="emis_mole_N2O_${MODEL_YEAR}_12US1_cmaq_cb05_soa_${MODEL_YEAR}ab_08c.${NETCDF_EXT_CMAQ}"
export AGSOIL_SOURCE_FP="${RESOURCE_ROOT}/${AGSOIL_PROJECT_NAME}/${AGSOIL_SOURCE_FN}"
export AGSOIL_TARGET_FN="agsoil_N2O_${MODEL_YEAR}.nc"
export AGSOIL_TARGET_DIR="${WORK_DIR}"
export AGSOIL_TARGET_FP="${AGSOIL_TARGET_DIR}/${AGSOIL_TARGET_FN}"

export NONSOIL_ANTHRO_PROJECT_NAME='EDGAR-4.2_minus_soil_and_biomass'
export NONSOIL_ANTHRO_SOURCE_FN="emis_mole_N2O_${MODEL_YEAR}_12US1_cmaq_cb05_soa_${MODEL_YEAR}ab_08c.${NETCDF_EXT_CMAQ}"
export NONSOIL_ANTHRO_SOURCE_FP="${RESOURCE_ROOT}/${NONSOIL_ANTHRO_PROJECT_NAME}/${NONSOIL_ANTHRO_SOURCE_FN}"
export NONSOIL_ANTHRO_TARGET_FN="nonsoil_N2O_${MODEL_YEAR}.nc"
export NONSOIL_ANTHRO_DIR="${WORK_DIR}"
export NONSOIL_ANTHRO_TARGET_FP="${NONSOIL_ANTHRO_DIR}/${NONSOIL_ANTHRO_TARGET_FN}"

export MARINE_PROJECT_NAME='GEIA_regrid'
export MARINE_SOURCE_FN="emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.${NETCDF_EXT_CMAQ}"
export MARINE_SOURCE_FP="${RESOURCE_ROOT}/${MARINE_PROJECT_NAME}/${MARINE_SOURCE_FN}"
export MARINE_TARGET_FN="marine_N2O_${MODEL_YEAR}.nc"
export MARINE_TARGET_DIR="${WORK_DIR}"
export MARINE_TARGET_FP="${MARINE_TARGET_DIR}/${MARINE_TARGET_FN}"

## monthly resources (one file/month)

export NATSOIL_PROJECT_NAME='CLM_CN_global_to_AQMEII-NA'
export NATSOIL_SOURCE_FN_TEMPLATE="emis_mole_N2O_${TEMPLATE_STRING}_12US1_cmaq_cb05_soa_2008ab_08c.${NETCDF_EXT_CMAQ}"
export NATSOIL_SOURCE_DIR="${RESOURCE_ROOT}/${NATSOIL_PROJECT_NAME}"
export NATSOIL_SOURCE_FP_TEMPLATE="${NATSOIL_SOURCE_DIR}/${NATSOIL_SOURCE_FN_TEMPLATE}"
export NATSOIL_TARGET_DIR="${WORK_DIR}"
export NATSOIL_TARGET_FN_TEMPLATE="natsoil_N2O_${TEMPLATE_STRING}.nc"
export NATSOIL_TARGET_FP_TEMPLATE="${NATSOIL_TARGET_DIR}/${NATSOIL_TARGET_FN_TEMPLATE}"

## daily resources (one file/day)

# original OAQPS emissions input to CDC/PHASE-2008
# export AQ_SOURCE_DIR='/work/MOD3APP/rtd/emis/2008cdc/input/nc' # on terrae
# export AQ_FN_TEMPLATE_ROOT="emis_mole_noN2O_${TEMPLATE_STRING}_12US1_cmaq_cb05_soa_2008ab_08c"
# 2008 emissions with "corrected" ag from Ryan Cleary via EJC Dec 2013 (slightly different species than previous)
export AQ_SOURCE_DIR='/work/MOD3APP/rtd/emis/2008cdc_ag_v2/AQonly/nc'
export AQ_FN_TEMPLATE_ROOT="emis_mole_noN2O_${TEMPLATE_STRING}_12US1_cmaq_cb05_F40_2008ab.ag_v2"
export AQ_SOURCE_FN_TEMPLATE="${AQ_FN_TEMPLATE_ROOT}.${NETCDF_EXT_NCL}"
export AQ_SOURCE_FP_TEMPLATE="${AQ_SOURCE_DIR}/${AQ_SOURCE_FN_TEMPLATE}"
export AQ_TARGET_DIR="${AQ_SOURCE_DIR}"
export AQ_TARGET_FN_TEMPLATE="${AQ_FN_TEMPLATE_ROOT}.${NETCDF_EXT_NCL}"
export AQ_TARGET_FP_TEMPLATE="${AQ_TARGET_DIR}/${AQ_TARGET_FN_TEMPLATE}"

export BURNING_PROJECT_NAME='GFED-3.1_global_to_AQMEII-NA'
export BURNING_SOURCE_FN_TEMPLATE="emis_mole_N2O_${TEMPLATE_STRING}.${NETCDF_EXT_CMAQ}"
export BURNING_SOURCE_DIR="${RESOURCE_ROOT}/${BURNING_PROJECT_NAME}"
export BURNING_SOURCE_FP_TEMPLATE="${BURNING_SOURCE_DIR}/${BURNING_SOURCE_FN_TEMPLATE}"
export BURNING_TARGET_DIR="${WORK_DIR}"
export BURNING_TARGET_FN_TEMPLATE="burning_N2O_${TEMPLATE_STRING}.nc"
export BURNING_TARGET_FP_TEMPLATE="${BURNING_TARGET_DIR}/${BURNING_TARGET_FN_TEMPLATE}"

## intermediate products: N2O integrations (of subprojects above)

export N2O_SUM_SOURCE_DIR='/work/MOD3APP/rtd/emis/2008N2O/AQMEII-NA_N2O_integration/N2Oonly' # on terrae
export N2O_SUM_FN_TEMPLATE_ROOT="emis_mole_N2O_${TEMPLATE_STRING}_12US1_cmaq_cb05_soa_2008ab_08c"
export N2O_SUM_SOURCE_FN_TEMPLATE="${N2O_SUM_FN_TEMPLATE_ROOT}.${NETCDF_EXT_NCL}"
export N2O_SUM_SOURCE_FP_TEMPLATE="${N2O_SUM_SOURCE_DIR}/${N2O_SUM_SOURCE_FN_TEMPLATE}"
# export N2O_SUM_TARGET_DIR="${WORK_DIR}"
export N2O_SUM_TARGET_DIR="${N2O_SUM_SOURCE_DIR}"
export N2O_SUM_TARGET_FN_TEMPLATE="${N2O_SUM_FN_TEMPLATE_ROOT}.${NETCDF_EXT_NCL}"
export N2O_SUM_TARGET_FP_TEMPLATE="${N2O_SUM_TARGET_DIR}/${N2O_SUM_TARGET_FN_TEMPLATE}"

## final outputs: sum of each hour's emissions by day (one file/day)

# export ALL_SUM_DIR="${WORK_DIR}"
export ALL_SUM_DIR='/work/MOD3APP/rtd/emis/2008N2O/AQMEII-NA_N2O_integration/AQplusN2O' # terrae
# Note: requires bash with string replacement (TODO: document)
export ALL_SUM_FN_TEMPLATE_ROOT="${AQ_FN_TEMPLATE_ROOT/noN2O/all}" # replace first match
# workaround NCL file-writing limitation
ALL_SUM_FN_TEMPLATE_CMAQ="${ALL_SUM_FN_TEMPLATE_ROOT}.${NETCDF_EXT_CMAQ}"
ALL_SUM_FN_TEMPLATE_NCL="${ALL_SUM_FN_TEMPLATE_ROOT}.${NETCDF_EXT_NCL}"
export ALL_SUM_FP_TEMPLATE_NCL="${ALL_SUM_DIR}/${ALL_SUM_FN_TEMPLATE_NCL}"
export ALL_SUM_FP_TEMPLATE_CMAQ="${ALL_SUM_DIR}/${ALL_SUM_FN_TEMPLATE_CMAQ}"
